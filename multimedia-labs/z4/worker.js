const channels = 4

function toGrayscale(pixels) {
    let r=0, g=0, b=0;
    for( let x = 0; x < pixels.length; x += channels ) {
        r += pixels[x]
        g += pixels[x + 1]
        b += pixels[x + 2]
    }
    r *= 3 / pixels.length
    g *= 3 / pixels.length
    b *= 3 / pixels.length

    for( let x = 0; x < pixels.length; x += channels ) {
        pixels[x] = r;
        pixels[x + 1] = g;
        pixels[x + 2] = b;
    }
}

addEventListener('message', ({ data }) => {
    toGrayscale(data.imageData.data);
    postMessage(data.imageData);
});