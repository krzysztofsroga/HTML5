const channels = 4

function toGrayscale(pixels, progress) {
    for( let x = 0; x < pixels.length; x += channels ) {
        let step = Math.floor(pixels.length / 100)
        if(x%step === 0) {
            postMessage( x / pixels.length)
        }
        let average = (
            pixels[x] +
            pixels[x + 1] +
            pixels[x + 2]
        ) / 3;

        pixels[x] = average;
        pixels[x + 1] = average;
        pixels[x + 2] = average;
    }
}

addEventListener('message', ({ data }) => {
    toGrayscale(data.imageData.data);
    postMessage(data.imageData);
});